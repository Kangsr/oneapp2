//
//  ContentView.swift
//  OneApp2
//
//  Created by Kang Risselada on 13/11/2019.
//  Copyright © 2019 O'Neill. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
            .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
